#ifndef QUALITY_CONTROL_H
#define QUALITY_CONTROL_H

#include <iostream>
#include <bitset>
#include <sys/stat.h>
#include "pluginLibrary/framework/Plugin.h"
#include "pluginLibrary/framework/FrameworkInterface.h"
#include "pluginLibrary/serializables/SerializableVector.h"


class QualityControl final : public Plugin {
private:
    const std::vector<std::string> statisticsVectorNames = {"means", "meansA", "meansC", "meansG", "meansT", "meansN", "median",
                                                            "qOne", "qThree", "dOne", "dNine", "min", "max"};
    const std::vector<std::string> countVectorNames = {"overallCount", "countA", "countC", "countG", "countT", "countN"};

    static inline unsigned char getBase(std::bitset<8> basecall) {
        return (unsigned char) (basecall.to_ulong() & 0b11u);
    };

    static inline unsigned char getQuality(std::bitset<8> basecall) {
        return (unsigned char) (basecall >> 2).to_ulong();
    };

    static inline float getMedian(const std::vector<unsigned char> &sortedQualities) {
        if (sortedQualities.size() % 2 == 0) {
            return (sortedQualities[sortedQualities.size() / 2] +
                    sortedQualities[(sortedQualities.size() / 2) - 1]) / 2.0;
        }
        return sortedQualities[sortedQualities.size() / 2];
    };

    static inline float getQThree(const std::vector<unsigned char> &sortedQualities) {
        if (sortedQualities.size() % 2 == 0) {
            std::vector<unsigned char> tmpVec(sortedQualities.begin() + sortedQualities.size() / 2,
                                              sortedQualities.end());
            return getMedian(tmpVec);
        }
        std::vector<unsigned char> tmpVec(sortedQualities.begin() + sortedQualities.size() / 2 + 1,
                                          sortedQualities.end());
        return getMedian(tmpVec);
    }

    static inline float getQOne(const std::vector<unsigned char> &sortedQualities) {
        if (sortedQualities.size() % 2 == 0) {
            std::vector<unsigned char> tmpVec(sortedQualities.begin(),
                                              sortedQualities.begin() + sortedQualities.size() / 2);
            return getMedian(tmpVec);
        }
        std::vector<unsigned char> tmpVec(sortedQualities.begin(),
                                          sortedQualities.begin() + sortedQualities.size() / 2);
        return getMedian(tmpVec);
    }

    static inline float getDOne(const std::vector<unsigned char> &sortedQualities) {
        return sortedQualities[(unsigned long) ceil(sortedQualities.size() / 10.0) - 1];
    }

    static inline float getDNine(const std::vector<unsigned char> &sortedQualities) {
        return sortedQualities[(unsigned long) ceil(sortedQualities.size() * 9 / 10.0) - 1];
    }

    template<typename T>
    inline std::vector<SerializableVector<T> *>
    getAllVectorsByName(const std::vector<std::string> &names, uint16_t lane) {
        std::vector<SerializableVector<T> *> tmpVectors;
        for (auto &name : names) {
            tmpVectors.push_back(
                    (SerializableVector<T> *) this->permanentFragment->getSerializable(
                            std::to_string(lane).append(name)));
        }
        return tmpVectors;
    }

    std::vector<SerializableVector<float> *> getAllStatisticsVectors(uint16_t lane) {
        return this->getAllVectorsByName<float>(this->statisticsVectorNames, lane);
    }


    std::vector<SerializableVector<unsigned long> *> getAllCountVectors(uint16_t lane) {
        return this->getAllVectorsByName<unsigned long>(this->countVectorNames, lane);
    }

    SerializableVector<float> *getTileQualities(uint16_t lane, uint16_t tile) {
        return (SerializableVector<float> *) this->permanentFragment->getSerializable(
                std::to_string(lane).append("means").append(std::to_string(tile)));
    }

    inline bool existsFile(const std::string& name){
        struct stat buffer;
        return (stat(name.c_str(), &buffer) == 0);
    }

    void sortQualities(std::vector<unsigned char> &qualities);

    void writeCSVHeaders();

    void writeCSVsFrom(int lastWrittenCycle);


#ifdef TEST_DEFINITIONS

    friend class QCAccessHelper;

#endif

public:
    void init() override;

    void setConfig() override;

    FragmentContainer *runCycle(FragmentContainer *inputFragments) override;

    void finalize() override;

    ~QualityControl() override = default;

    void writeConfigJson();
};

#endif //QUALITY_CONTROL_H

