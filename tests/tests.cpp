#include <bitset>
#include "catch2/catch.hpp"
#include "../QualityControl.h"

using namespace std;

class QCAccessHelper {
//private:
    //QualityControl &qualityControl;
public:
    //explicit QCAccessHelper(QualityControl &qc) : qualityControl(qc) { this->qualityControl.setConfig(); };

    static inline unsigned char getBase(bitset<8> basecall) {
        return QualityControl::getBase(basecall);
    };

    static inline unsigned long getQuality(bitset<8> basecall) {
        return QualityControl::getQuality(basecall);
    };

    //QualityControl &getQC() { return this->qualityControl; };
};

TEST_CASE("QualityControlPlugin") {
    //QCAccessHelper qcAccessHelper(QualityControl);

    SECTION("Should parse the correct base from a Byte") {

        bitset<8> byte;
        int base;

        byte = 0b00000000;
        base = QCAccessHelper::getBase(byte);
        REQUIRE(base == 0);

        byte = 0b00000001;
        base = QCAccessHelper::getBase(byte);
        REQUIRE(base == 1);

        byte = 0b00000010;
        base = QCAccessHelper::getBase(byte);
        REQUIRE(base == 2);

        byte = 0b00000011;
        base = QCAccessHelper::getBase(byte);
        REQUIRE(base == 3);

        byte = 0b11111100;
        base = QCAccessHelper::getBase(byte);
        REQUIRE(base == 0);
    }

    SECTION("Should parse the correct quality from a Byte") {

        std::bitset<8> byte;
        unsigned long quality;

        byte = 0b00000000;
        quality = QCAccessHelper::getQuality(byte);
        REQUIRE(quality == 0);

        byte = 0b10100001;
        quality = QCAccessHelper::getQuality(byte);
        REQUIRE(quality == 40);

        byte = 0b00101010;
        quality = QCAccessHelper::getQuality(byte);
        REQUIRE(quality == 10);

        byte = 0b01010011;
        quality = QCAccessHelper::getQuality(byte);
        REQUIRE(quality == 20);
    }
}