#!/usr/bin/env bash

# move the plugin binary to the correct directory
mv -u plugin/libQCPlugin.so ../plugins/binary/libQCPlugin.so

# move the frontend code
mv -u qualityControl/ ../plugins/qualityControl

# move the standard config
mv -u config/qualitycontrol.json ../config/qualitycontrol.json

rm -r ../qualityControl/