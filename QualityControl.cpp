#include <bitset>
#include <numeric>

#include "QualityControl.h"
#include "pluginLibrary/framework/fragments/Fragment.cpp"
#include "json.hpp"

using json = nlohmann::json;

using namespace std;

/**
 * This function is used to instantiate the Plugin as the framework
 * @returns a pointer to a new instance of the `TestPlugin` class
 */
extern "C" QualityControl *create() {
    return new QualityControl;
}
/**
 * This function deletes the given plugin pointer to free its memory
 */
extern "C" void destroy(QualityControl *plugin) {
    delete plugin;
}

FragmentContainer *QualityControl::runCycle(FragmentContainer *inputFragments) {
    (void) inputFragments;

        for (auto &lane : this->getConfigEntry < vector < uint16_t >> ("lanes")) {
            unsigned long long overallQuality = 0;
            unsigned long overallCount = 0;
            unsigned long baseCounts[5] = {0};
            unsigned long long baseQualities[5] = {0};
            vector<unsigned char> allQualities;

            for (auto &tile : this->getConfigEntry < vector < uint16_t >> ("tiles")) {
                auto &bcl = this->framework->getBcl(lane, tile, 0);
                unsigned long long tileQuality = 0;

                for (unsigned int i = 0; i < bcl.size(); i++) {
                    if (!this->framework->filterBasecall(lane, tile, i)) {
                        auto basecall = bcl[i];
                        unsigned char currentBase = basecall & 0b11u;
                        unsigned char currentQuality = basecall >> 2u;
                        currentBase = (currentBase == 0 && currentQuality == 0 ? 4 : currentBase);
                        tileQuality += currentQuality;
                        baseCounts[currentBase] += 1;
                        baseQualities[currentBase] += currentQuality;

                        allQualities.emplace_back(currentQuality);
                    }
                }

                auto tileQualities = this->getTileQualities(lane, tile);
                tileQualities->emplace_back(tileQuality / (float) bcl.size());

                overallQuality += tileQuality;
                overallCount += bcl.size();
            }

            this->sortQualities(allQualities);

            auto meanQualityOfCycle = overallQuality / (float) overallCount;

            auto statisticsVector = this->getAllStatisticsVectors(lane);

            statisticsVector[0]->emplace_back(meanQualityOfCycle);
            statisticsVector[1]->emplace_back(baseQualities[0] / (float) baseCounts[0]);
            statisticsVector[2]->emplace_back(baseQualities[1] / (float) baseCounts[1]);
            statisticsVector[3]->emplace_back(baseQualities[2] / (float) baseCounts[2]);
            statisticsVector[4]->emplace_back(baseQualities[3] / (float) baseCounts[3]);
            statisticsVector[5]->emplace_back(baseCounts[4] == 0 ? 0 : baseQualities[4] / (float) baseCounts[4]);

            statisticsVector[6]->emplace_back(QualityControl::getMedian(allQualities));
            statisticsVector[7]->emplace_back(QualityControl::getQOne(allQualities));
            statisticsVector[8]->emplace_back(QualityControl::getQThree(allQualities));
            statisticsVector[9]->emplace_back(QualityControl::getDOne(allQualities));
            statisticsVector[10]->emplace_back(QualityControl::getDNine(allQualities));
            statisticsVector[11]->emplace_back(allQualities.front());
            statisticsVector[12]->emplace_back(allQualities.back());

            auto countsVector = this->getAllCountVectors(lane);
            countsVector[0]->emplace_back(overallCount);
            countsVector[1]->emplace_back(baseCounts[0]);
            countsVector[2]->emplace_back(baseCounts[1]);
            countsVector[3]->emplace_back(baseCounts[2]);
            countsVector[4]->emplace_back(baseCounts[3]);
            countsVector[5]->emplace_back(baseCounts[4]);

            if (this->getConfigEntry<bool>("showOutput"))
                cout << "Mean quality of lane " << lane << " for cycle " << this->framework->getCurrentCycle() << ": "
                     << meanQualityOfCycle << endl;

        }

        if (this->getConfigEntry<bool>("printAfterEveryCycle")) {
            writeCSVsFrom(this->framework->getCurrentCycle() - 1);
        } else {

            auto vec = this->getConfigEntry < vector < uint16_t >> ("printAfterCycle");
            for(int i = 0; i < vec.size(); i++){
                if(vec[i] == this->framework->getCurrentCycle())
                    writeCSVsFrom(i == 0 ? 0 : vec[i-1]);
            }
        }

    return this->framework->createNewFragmentContainer();
}

void QualityControl::setConfig() {
    this->registerConfigEntry < vector < uint16_t > , string > ("printAfterCycle", "", Configurable::toVector<uint16_t>(
            ','), EntryOptions::NO_WARNING);
    this->registerConfigEntry<bool>("printAfterEveryCycle", true, EntryOptions::NO_WARNING);
    this->registerConfigEntry<string>("outputDirectory", "../plugins/qualityControl/");
    this->registerConfigEntry<int>("reloadTimeout", 300000); // 300000 => 5min
    this->registerConfigEntry<bool>("showOutput", false, EntryOptions::NO_WARNING);

    auto outputDirectory = this->getConfigEntry<string>("outputDirectory");

    if(!existsFile(outputDirectory)){
        mkdir(outputDirectory.c_str(), 777);
    }
}

void QualityControl::init() {
    this->permanentFragment = this->framework->createNewFragment("permanent");

    for (auto &lane : this->getConfigEntry < vector < uint16_t >> ("lanes")) {
        for (auto &name : this->statisticsVectorNames) {
            auto *vec = new SerializableVector<float>();
            this->permanentFragment->setSerializable(to_string(lane).append(name), vec);
        }
        for (auto &name : this->countVectorNames) {
            auto *vec = new SerializableVector<unsigned long>();
            this->permanentFragment->setSerializable(to_string(lane).append(name), vec);
        }
        for (auto &tile : this->getConfigEntry < vector < uint16_t >> ("tiles")) {
            auto *vec = new SerializableVector<float>();
            this->permanentFragment->setSerializable(
                    to_string(lane).append("means").append(to_string(tile)), vec);
        }
    }

    this->writeCSVHeaders();

    this->writeConfigJson();
}

void QualityControl::finalize() {
    if (!this->getConfigEntry<bool>("printAfterEveryCycle")) {
        auto vec = this->getConfigEntry < vector < uint16_t >> ("printAfterCycle");
        if (!vec.empty())
            writeCSVsFrom(vec.back());
        else
            writeCSVsFrom(0);
    }
}

void QualityControl::sortQualities(std::vector<unsigned char> &qualities) {
    unsigned long qualityCount[41] = {0};

    for (auto &q : qualities) {
        qualityCount[q]++;
    }

    unsigned int offset = 0;
    for (unsigned char i = 0; i < 41; i++) {
        for (unsigned int j = 0; j < qualityCount[i]; j++) {
            qualities[offset + j] = i;
        }
        offset += qualityCount[i];
    }
}

void QualityControl::writeCSVHeaders() {
    ofstream dataFile;

    for (auto &lane : this->getConfigEntry < vector < uint16_t >> ("lanes")) {
        dataFile.open(this->getConfigEntry<string>("outputDirectory") + "qualityReport" + to_string(lane) + ".csv");

        for (auto &name : this->statisticsVectorNames) {
            dataFile << name << ",";
        }
        for (auto &name : this->countVectorNames) {
            dataFile << name << ",";
        }

        for (auto &tile : this->getConfigEntry < vector < uint16_t >> ("tiles")) {
            dataFile << "means" << tile << ",";
        }
        dataFile << endl;
        dataFile.close();
    }

}

void QualityControl::writeCSVsFrom(int lastWrittenCycle) {
    ofstream dataFile;

    for (auto &lane : this->getConfigEntry < vector < uint16_t >> ("lanes")) {
        dataFile.open(this->getConfigEntry<string>("outputDirectory") + "qualityReport" + to_string(lane) + ".csv",
                      ios_base::app);

        auto statisticsVector = this->getAllStatisticsVectors(lane);
        auto countVectors = this->getAllCountVectors(lane);

        for (unsigned long i = lastWrittenCycle; i < statisticsVector[0]->size(); i++) {
            dataFile << statisticsVector[0]->at(i) << ",";
            dataFile << statisticsVector[1]->at(i) << ",";
            dataFile << statisticsVector[2]->at(i) << ",";
            dataFile << statisticsVector[3]->at(i) << ",";
            dataFile << statisticsVector[4]->at(i) << ",";
            dataFile << statisticsVector[5]->at(i) << ",";
            dataFile << statisticsVector[6]->at(i) << ",";
            dataFile << statisticsVector[7]->at(i) << ",";
            dataFile << statisticsVector[8]->at(i) << ",";
            dataFile << statisticsVector[9]->at(i) << ",";
            dataFile << statisticsVector[10]->at(i) << ",";
            dataFile << statisticsVector[11]->at(i) << ",";
            dataFile << statisticsVector[12]->at(i) << ",";

            dataFile << countVectors[0]->at(i) << ",";
            dataFile << countVectors[1]->at(i) << ",";
            dataFile << countVectors[2]->at(i) << ",";
            dataFile << countVectors[3]->at(i) << ",";
            dataFile << countVectors[4]->at(i) << ",";
            dataFile << countVectors[5]->at(i) << ",";

            for (auto &tile : this->getConfigEntry < vector < uint16_t >> ("tiles")) {
                auto tmpVec = this->getTileQualities(lane, tile);
                dataFile << tmpVec->at(i) << ",";
            }

            dataFile << endl;
        }
        dataFile.close();

    }
}

void QualityControl::writeConfigJson() {
    json config = {
            {"existingLanes", this->getConfigEntry < vector < uint16_t >> ("lanes")},
            {"numCycles",     this->framework->getCycleCount()},
            {"reloadTimeout", this->getConfigEntry<int>("reloadTimeout")}
    };

    ofstream configFile;
    configFile.open(this->getConfigEntry<string>("outputDirectory") + "config.json");
    configFile << setw(4) << config << endl;
    configFile.close();
}




