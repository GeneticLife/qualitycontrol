/* exported BaseQuality */

class BaseQuality extends Chart {
	constructor() {
		super();
		const container = d3.select('#boxplot_container');

		const svgWidth = parseInt(container.style('width'));
		const svgHeight = parseInt(container.style('height'));

		this.svg = d3
			.select('#boxplot')
			.attr('width', svgWidth)
			.attr('height', svgHeight);
		this.margin = 50;

		this.width = svgWidth - this.margin - this.margin; // Left + right
		this.height = svgHeight - this.margin - this.margin - 20; // Top + bottom

		this.initChartArea();
	}

	initChartArea() {
		this.rootGroup = this.svg
			.append('g')
			.attr('transform', translateBy(this.margin, this.margin));

		this.scaleX = d3.scaleLinear().range([0, this.width]);
		this.scaleY = d3
			.scaleLinear()
			.range([this.height, 0])
			.domain([0, 40])
			.nice();
		this.scaleBand = d3.scaleBand().paddingInner(0.05);

		this.rootGroup.append('g').call(d3.axisLeft(this.scaleY));

		this.rootGroup
			.append('text')
			.attr('transform', 'rotate(-90)')
			.attr('y', 0 - this.margin)
			.attr('x', 0 - this.height / 2)
			.attr('dy', '1em')
			.attr('fill', 'currentColor')
			.style('text-anchor', 'middle')
			.text('Quality');

		this.rootGroup
			.append('text')
			.attr('transform', translateBy(this.width / 2, this.height + 40))
			.attr('fill', 'currentColor')
			.style('text-anchor', 'middle')
			.text('Cycle');
	}

	processData(data, min, max) {
		if (min === undefined) {
			min = 1;
		}

		if (max === undefined) {
			max = data.length;
		}

		data = data.filter((x, i) => i >= min - 1 && i <= max - 1);

		this.scaleX.domain([min - 0.5, max + 1.5]);
		this.scaleBand
			.range([0, this.scaleX.range()[1]])
			.domain(d3.range(min, max + 2));

		let numberOfTicks = data.length / 10;
		numberOfTicks = numberOfTicks < 20 ? numberOfTicks * 2 : numberOfTicks;
		numberOfTicks = numberOfTicks < 10 ? data.length : numberOfTicks;

		this.rootGroup
			.append('g')
			.attr('transform', translateBy(0, this.height))
			.call(d3.axisBottom(this.scaleX).ticks(numberOfTicks));

		this.drawBackgroundRects(data, 0, 20, '#ECB263', min);

		this.drawBackgroundRects(data, 20, 28, '#A6BC61', min);

		this.drawBackgroundRects(data, 28, 40, '#60B8D6', min);

		this.rootGroup
			.append('g')
			.selectAll('rect')
			.data(data)
			.enter()
			.append('rect')
			.attr('x', (d, i) => this.scaleBand(i + min))
			.attr('y', 0)
			.attr('width', this.scaleBand.bandwidth())
			.attr('height', this.scaleY(0) - this.scaleY(40))
			.style('pointer-events', 'all')
			.attr('opacity', 0)
			.on('mouseover', (d, i) => this.showTooltip(d, i + min))
			.on('mouseout', () => this.hideTooltip());

		this.minCircles = this.rootGroup
			.append('g')
			.attr('fill', '#BE1622')
			.selectAll('path')
			.data(data)
			.join('path')
			.attr('transform', (d, i) =>
				translateBy(this.scaleX(i + min), this.scaleY(d.min))
			)
			.attr('d', d =>
				d3.arc()({
					innerRadius: 0,
					outerRadius: this.scaleBand.bandwidth() / 2,
					startAngle: this.cutsAxis(d, true) ? -Math.PI * 0.5 : 0,
					endAngle: this.cutsAxis(d, true) ? Math.PI * 0.5 : 2 * Math.PI
				})
			)
			.style('pointer-events', 'none');

		this.maxCircles = this.rootGroup
			.append('g')
			.attr('fill', '#006633')
			.selectAll('path')
			.data(data)
			.join('path')
			.attr('transform', (d, i) =>
				translateBy(this.scaleX(i + min), this.scaleY(d.max))
			)
			.attr('d', d =>
				d3.arc()({
					innerRadius: 0,
					outerRadius: this.scaleBand.bandwidth() / 2,
					startAngle: this.cutsAxis(d, false) ? Math.PI * 0.5 : 0,
					endAngle: this.cutsAxis(d, false) ? Math.PI * 1.5 : 2 * Math.PI
				})
			)
			.style('pointer-events', 'none');

		this.qOneRects = this.rootGroup
			.append('g')
			.selectAll('rect')
			.data(data)
			.enter()
			.append('rect')
			.attr('x', (d, i) => this.scaleBand(i + min))
			.attr('y', d => this.scaleY(d.median))
			.attr('width', this.scaleBand.bandwidth())
			.attr('height', d => this.scaleY(d.qOne) - this.scaleY(d.median))
			.attr('stroke', 'black')
			.attr('stroke-width', 0.5)
			.style('fill', '#005575')
			.style('pointer-events', 'none');

		this.qThreeRects = this.rootGroup
			.append('g')
			.selectAll('rect')
			.data(data)
			.enter()
			.append('rect')
			.attr('x', (d, i) => this.scaleBand(i + min))
			.attr('y', d => this.scaleY(d.qThree))
			.attr('width', this.scaleBand.bandwidth())
			.attr('height', d => this.scaleY(d.median) - this.scaleY(d.qThree))
			.attr('stroke', 'black')
			.attr('stroke-width', 0.5)
			.style('fill', '#005575')
			.style('pointer-events', 'none');

		this.medianLines = this.rootGroup
			.append('g')
			.selectAll('line')
			.data(data)
			.enter()
			.append('line')
			.attr('stroke', '#FFED00')
			.attr('stroke-width', 1.5)
			.attr('x1', (d, i) => this.scaleBand(i + min))
			.attr(
				'x2',
				(d, i) => this.scaleBand(i + min) + this.scaleBand.bandwidth()
			)
			.attr('y1', d => this.scaleY(d.median))
			.attr('y2', d => this.scaleY(d.median));

		this.dOneWhiskers = this.drawWhiskers(data, 'dOne', 'qOne', min, false);

		this.dNineWhiskers = this.drawWhiskers(data, 'qThree', 'dNine', min, true);

		this.averagePath = this.rootGroup
			.append('path')
			.datum(data)
			.attr('fill', 'none')
			.attr('stroke', '#E6007E')
			.attr('stroke-width', 1.5)
			.attr(
				'd',
				d3
					.line()
					.x((d, i) => this.scaleX(i + min))
					.y(d => this.scaleY(d.means))
			);
	}

	remove() {
		this.rootGroup.remove();
	}

	drawBackgroundRects(data, lowerBound, upperBound, color, min) {
		this.rootGroup
			.append('g')
			.selectAll('rect')
			.data(data)
			.enter()
			.append('rect')
			.attr('x', (d, i) => this.scaleBand(i + min))
			.attr('y', this.scaleY(upperBound))
			.attr('width', this.scaleBand.bandwidth())
			.attr('height', this.scaleY(lowerBound) - this.scaleY(upperBound))
			.style('fill', color)
			.attr('opacity', (d, i) => (i % 2 === 0 ? 0.9 : 1));
	}

	drawWhiskers(data, lowerValue, upperValue, min, up) {
		const whiskerGroup = this.rootGroup.append('g');
		whiskerGroup
			.append('g')
			.selectAll('line')
			.data(data)
			.enter()
			.append('line')
			.attr('stroke', 'black')
			.attr('stroke-dasharray', '5,5')
			.attr('stroke-width', 0.5)
			.attr('x1', (d, i) => this.scaleX(i + min))
			.attr('x2', (d, i) => this.scaleX(i + min))
			.attr('y1', d => this.scaleY(d[lowerValue]))
			.attr('y2', d => this.scaleY(d[upperValue]));

		whiskerGroup
			.append('g')
			.selectAll('line')
			.data(data)
			.enter()
			.append('line')
			.attr('stroke', 'black')
			.attr('stroke-width', 1.5)
			.attr('opacity', d => (d[upperValue] - d[lowerValue] > 0 ? 1 : 0))
			.attr('x1', (d, i) => this.scaleBand(i + min))
			.attr(
				'x2',
				(d, i) => this.scaleBand(i + min) + this.scaleBand.bandwidth()
			)
			.attr('y1', d => this.scaleY(up ? d[upperValue] : d[lowerValue]))
			.attr('y2', d => this.scaleY(up ? d[upperValue] : d[lowerValue]));

		return whiskerGroup;
	}

	showTooltip(dataEl, idx) {
		const leftOffset =
			d3.event.pageX + 140 + this.scaleBand.step() >= window.innerWidth - 25
				? d3.event.pageX - 140
				: d3.event.pageX + this.scaleBand.step(); // Idk why this is needed but if I inline it, it doesnt work
		baseTooltip
			.transition()
			.duration(200)
			.style('opacity', 0.9);
		baseTooltip
			.html(
				`Cycle ${idx}<br />Max: ${dataEl.max}<br />9th Decile: ${
					dataEl.dNine
				}<br />Upper Quartile: ${dataEl.qThree}<br />Median: ${
					dataEl.median
				}<br />Lower Quartile: ${dataEl.qOne}<br />1st Decile: ${
					dataEl.dOne
				}<br />Min: ${dataEl.min}<br />Mean: ${dataEl.means.toFixed(2)}`
			)
			.style('left', leftOffset + 'px')
			.style('top', d3.event.pageY - 32 + 'px');
	}

	hideTooltip() {
		baseTooltip
			.transition()
			.duration(500)
			.style('opacity', 0);
	}

	cutsAxis(dataEl, comapareMin) {
		if (comapareMin)
			return (
				this.scaleY(dataEl.min) + this.scaleBand.bandwidth() / 2 >= this.height
			);
		return this.scaleY(dataEl.max) - this.scaleBand.bandwidth() / 2 < 0;
	}
}
