/* exported TileChart */

class TileChart extends Chart {
	constructor() {
		super();
		const container = d3.select('#tile_chart_container');

		const svgWidth = parseInt(container.style('width'));
		this.svgHeight = parseInt(container.style('height'));

		this.svg = d3
			.select('#tile_chart')
			.attr('width', svgWidth)
			.attr('height', this.svgHeight);

		this.margin = 50;

		this.width = svgWidth - this.margin - this.margin - 20; // Left + right
		this.height = this.svgHeight - this.margin - this.margin - 20; // Top + bottom

		this.initChartArea();
	}

	initChartArea() {
		this.rootGroup = this.svg
			.append('g')
			.attr('transform', translateBy(this.margin, this.margin));

		this.rootGroup
			.append('text')
			.attr('transform', 'rotate(-90)')
			.attr('y', 0 - this.margin)
			.attr('x', 0 - this.height / 2)
			.attr('dy', '1em')
			.attr('fill', 'currentColor')
			.style('text-anchor', 'middle')
			.text('Tiles');

		this.rootGroup
			.append('text')
			.attr(
				'transform',
				'translate(' + this.width / 2 + ' ,' + (this.height + 40) + ')'
			)
			.attr('fill', 'currentColor')
			.style('text-anchor', 'middle')
			.text('Cycle');

		this.xAxis = d3.scaleLinear().range([0, this.width]);
		this.scaleX = d3
			.scaleBand()
			.paddingInner(0)
			.range([0, this.width]);
		this.scaleY = d3
			.scaleBand()
			.paddingInner(0.05)
			.range([this.height, 0]);
		this.colorScale = d3
			.scaleLinear()
			.domain([0, 28, 40])
			.range(['#F39200', '#95C11F', '#009FE3']);

		this.defColorGradient();

		this.legend = this.rootGroup
			.append('g')
			.attr('transform', translateBy(this.width + 20, 0));

		this.legend
			.append('text')
			.style('text-anchor', 'middle')
			.style('fill', 'currentColor')
			.text('Quality')
			.attr('x', 5)
			.attr('y', -5);

		this.legend
			.append('rect')
			.attr('x', 0)
			.attr('y', -this.height)
			.attr('width', 15)
			.attr('height', this.height - 5)
			.attr('stroke', 'none')
			.attr('transform', 'scale(1,-1)')
			.style('fill', 'url(#colorGradient)');

		const linScaleY = d3
			.scaleLinear()
			.domain([0, 40])
			.range([this.height - 5, 0]);
		this.legend
			.append('g')
			.selectAll('text')
			.data(this.colorScale.domain())
			.enter()
			.append('text')
			.attr('x', 20)
			.attr('y', d => linScaleY(d) + 10)
			.style('fill', 'currentColor')
			.text(d => `${d}`);
	}

	processData(data, min, max) {
		if (min === undefined) {
			min = 1;
		}

		if (max === undefined) {
			max = data.length;
		}

		this.xAxis.domain([min - 0.5, max + 1.5]);
		this.scaleX.domain(d3.range(min, max + 2));
		const tiles = data.columns
			.filter(col => col.match(/means\d/))
			.map(tileName => tileName.replace('means', ''));
		this.scaleY.domain(tiles);

		if (this.scaleY.step() < 15) {
			this.rescaleChartHeight(data, min, max);
			return;
		}

		this.rootGroup
			.append('g')
			.attr('transform', translateBy(0, this.height))
			.call(d3.axisBottom(this.xAxis).ticks(data.length / 10));

		this.rootGroup.append('g').call(d3.axisLeft(this.scaleY));

		for (const tile of tiles) {
			this.createRects(
				data.filter((x, i) => i >= min - 1 && i <= max - 1),
				this.rootGroup,
				tile,
				min
			);
		}
	}

	createRects(data, rootGroup, tileIndex, min) {
		rootGroup
			.append('g')
			.selectAll('rect')
			.data(data)
			.enter()
			.append('rect')
			.attr('x', (d, i) => this.scaleX(i + min))
			.attr('y', this.scaleY(tileIndex))
			.attr('width', this.scaleX.bandwidth())
			.attr('height', this.scaleY.bandwidth())
			.attr('stroke', 'none')
			.style('fill', d => this.colorScale(d[`means${tileIndex}`]))
			.on('mouseover', (d, i) => this.showTooltip(d, i + min))
			.on('mouseout', () => this.hideTooltip());
	}

	showTooltip(dataEl, idx) {
		const tileIdx = Math.floor(
			d3.mouse(d3.event.target)[1] / this.scaleY.step()
		);
		const tile = this.scaleY.domain()[
			this.scaleY.domain().length - tileIdx - 1
		];
		const leftOffset =
			d3.event.pageX + 140 >= window.innerWidth - 25
				? d3.event.pageX - 140
				: d3.event.pageX; // Idk why this is needed but if I inline it, it doesnt work
		tileTooltip
			.transition()
			.duration(200)
			.style('opacity', 0.9);
		tileTooltip
			.html(`Cycle ${idx}: ${dataEl[`means${tile}`].toFixed(2)}`)
			.style('left', leftOffset + 'px')
			.style('top', d3.event.pageY - 32 + 'px');
	}

	hideTooltip() {
		tileTooltip
			.transition()
			.duration(500)
			.style('opacity', 0);
	}

	remove() {
		this.rootGroup.remove();
		this.svg.attr('height', this.svgHeight);
		this.height = this.svgHeight - this.margin - this.margin - 20;
	}

	rescaleChartHeight(data, min, max) {
		this.remove();
		this.height = this.scaleY.domain().length * 15;
		this.svg.attr('height', this.height + 2 * this.margin + 20);
		this.initChartArea();
		this.processData(data, min, max);
	}

	defColorGradient() {
		const colorGradient = this.svg
			.append('defs')
			.append('linearGradient')
			.attr('id', 'colorGradient')
			.attr('gradientTransform', 'rotate(90)');
		for (const el of this.colorScale.domain()) {
			colorGradient
				.append('stop')
				.attr('offset', `${(el * 100) / 40}%`)
				.attr('stop-color', this.colorScale(el));
		}
	}
}
