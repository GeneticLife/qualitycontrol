/* exported BaseProportion */

class BaseProportion extends Chart {
	constructor() {
		super();
		this.margin = 50;

		const container = d3.select('#base_proportion_container');

		const svgWidth = parseInt(container.style('width'));
		const svgHeight = parseInt(container.style('height'));

		this.svg = d3
			.select('#base_proportion')
			.attr('width', svgWidth)
			.attr('height', svgHeight);

		this.width = svgWidth - this.margin - this.margin; // Left + right
		this.height = svgHeight - this.margin - this.margin - 20; // Top + bottom

		this.initChartArea();
	}

	initChartArea() {
		this.rootGroup = this.svg
			.append('g')
			.attr('transform', translateBy(this.margin, this.margin));

		this.rootGroup
			.append('text')
			.attr('transform', 'rotate(-90)')
			.attr('y', 0 - this.margin)
			.attr('x', 0 - this.height / 2)
			.attr('dy', '1em')
			.attr('fill', 'currentColor')
			.style('text-anchor', 'middle')
			.text('Proportion');

		this.rootGroup
			.append('text')
			.attr(
				'transform',
				'translate(' + this.width / 2 + ' ,' + (this.height + 40) + ')'
			)
			.attr('fill', 'currentColor')
			.style('text-anchor', 'middle')
			.text('Cycle');

		this.scaleX = d3.scaleLinear().range([0, this.width]);
		this.scaleY = d3
			.scaleLinear()
			.range([0, this.height])
			.domain([100, 0]);
		this.ordinalScale = d3
			.scaleOrdinal()
			.domain(['A', 'C', 'G', 'T', 'N', 'CG'])
			.range(['#4BA825', 'yellow', 'blue', 'violet', 'red', 'white']);

		this.rootGroup.append('g').call(d3.axisLeft(this.scaleY));
	}

	processData(data, min, max) {
		if (min === undefined) {
			min = 1;
		}

		if (max === undefined) {
			max = data.length;
		}

		data = data.filter((x, i) => i >= min - 1 && i <= max - 1);

		this.scaleX.domain([min - 0.5, max + 1.5]);

		this.rootGroup
			.append('g')
			.attr('transform', translateBy(0, this.height))
			.call(d3.axisBottom(this.scaleX).ticks(data.length / 10));

		this.rootGroup
			.append('rect')
			.datum(data)
			.attr('x', 0)
			.attr('y', 0)
			.attr('width', this.width)
			.attr('height', this.height)
			.style('visibility', 'hidden')
			.style('pointer-events', 'all')
			.on('mouseover', d => this.movePositionLine(d, min))
			.on('mousemove', d => this.movePositionLine(d, min))
			.on('mouseleave', d => this.removePositionLine(d));

		const texts = this.rootGroup
			.append('g')
			.attr('font-size', '15px')
			.attr('fill-opacity', 0);

		this.legend = this.rootGroup
			.append('g')
			.attr('transform', translateBy(this.width - 50, 10))
			.attr('font-size', '18px')
			.attr('fill-opacity', 1);

		this.ordinalScale.domain().forEach((base, i) =>
			this.legend
				.append('text')
				.attr('fill', this.ordinalScale(base))
				.attr('y', i * 17)
				.text(`%${base}`)
		);

		this.movingBaseTexts = [];
		for (const base of this.ordinalScale.domain()) {
			this.rootGroup
				.append('path')
				.datum(data)
				.attr('fill', 'none')
				.attr('stroke', this.ordinalScale(base))
				.attr('d', this.getLineGeneratorForBase(base, min))
				.style('pointer-events', 'none');

			this.movingBaseTexts.push(
				texts.append('text').attr('fill', this.ordinalScale(base))
			);
		}

		this.cycleText = texts.append('text').attr('fill', '#F39200');

		this.positionLine = this.rootGroup
			.append('line')
			.attr('stroke', '#F39200')
			.attr('stroke-width', 2)
			.attr('opacity', 0);
	}

	getLineGeneratorForBase(base, min) {
		if (base === 'CG') {
			return d3
				.line()
				.x((d, i) => this.scaleX(i + min))
				.y(d => this.scaleY(((d.countC + d.countG) / d.overallCount) * 100));
		}

		return d3
			.line()
			.x((d, i) => this.scaleX(i + min))
			.y(d => this.scaleY((d[`count${base}`] / d.overallCount) * 100));
	}

	movePositionLine(data, min) {
		const coordinates = d3.mouse(d3.event.target);

		this.legend.attr('fill-opacity', 0);

		this.positionLine
			.attr('opacity', 1)
			.attr('x1', coordinates[0])
			.attr('x2', coordinates[0])
			.attr('y1', this.scaleY(100))
			.attr('y2', this.scaleY(0));

		this.cycleText
			.attr('fill-opacity', 1)
			.attr('x', coordinates[0] + 5)
			.attr('y', this.scaleY(100) + 15)
			.text(
				`Cycle ${Math.round(this.scaleX.invert(d3.mouse(d3.event.target)[0]))}`
			);

		this.showBaseProportions(data, coordinates, min);
	}

	removePositionLine() {
		const coordinates = d3.mouse(d3.event.target);
		if (
			coordinates[1] - 5 < 0 ||
			coordinates[1] + 5 > this.height ||
			coordinates[0] - 5 < 0 ||
			coordinates[0] + 5 > this.width
		) {
			this.positionLine.attr('opacity', 0);
			this.cycleText.attr('fill-opacity', 0);
			for (const text of this.movingBaseTexts) {
				text.attr('fill-opacity', 0);
			}

			this.legend.attr('fill-opacity', 1);
		}
	}

	showBaseProportions(data, coordinates, min) {
		const idx = Math.round(this.scaleX.invert(coordinates[0])) - min;
		const dataEl = data[idx];
		for (let i = 0; i < this.movingBaseTexts.length; i++) {
			const base = this.ordinalScale.domain()[i];
			this.movingBaseTexts[i]
				.attr('x', coordinates[0] + 5)
				.attr('y', this.scaleY(100) + 32 + i * 17)
				.attr('fill-opacity', 1)
				.text(`${this.getValueFor(dataEl, base)} %${base}`);
		}
	}

	getValueFor(dataEl, base) {
		if (dataEl === undefined) {
			return NaN;
		}

		if (base === 'CG') {
			return (
				((dataEl.countC + dataEl.countG) / dataEl.overallCount) *
				100
			).toFixed(2);
		}

		return ((dataEl[`count${base}`] / dataEl.overallCount) * 100).toFixed(2);
	}

	remove() {
		this.rootGroup.remove();
	}
}
