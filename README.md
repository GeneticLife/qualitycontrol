# QualityControl

## LiveKit Plugin
QualityControl is a plugin written for the excellent framework [LiveKit](https://gitlab.com/GeneticLife/LiveKit). It enables you to monitor the quality of your Illumina sequencing run in real-time.

## Installation
The easiest way to install the plugin is to download this [archive](https://gitlab.com/GeneticLife/qualitycontrol/wikis/uploads/a13622feccad29f0c0f00fe6cc0148ae/qualityControl.tar.xz) and to unzip it in the top-level folder of LiveKit. Run the `install.sh` script in the folder and you should be good to go.

If you want to install it by source, you'll need to clone the repo and build the project. Afterwards you need to move the `charts` directory to LiveKit and adapt the config path as necessary.

## Configuration options

- `printAfterEveryCycle: bool` - set to true to update the output csv after every analyzed cycle - default: `true`
- `printAfterCycle: vector<int>` - if `printAfterEveryCycle` is disabled, one can control when LiveQC should output quality data. LiveQC will always output after the last cycle was run.
- `outputDirectory: string` - specify the directory where the charts are, so LiveQC can update the data. - default: `../plugins/qualityControl/`
- `reloadTimeout: int` - specify the timeout in ms after which the data is refreshed and the charts reloaded. - default: `300000` = 5min
- `showOutput: bool` - set to true to see cmd output when LiveQC is running - default: `false`

## Frontend

I used the following dependencies: 
- [d3](https://d3js.org/) to create the charts.
- [bootstrap](https://getbootstrap.com/) to create the buttons and dropdowns.
- [noUiSlider](https://github.com/leongersen/noUiSlider) to create the slider.
- [xo-linter](https://github.com/xojs/xo) to prettify and check my code.
